+++
widget = "pages"
headless = true
active = true
weight = 30
title = "Entradas del Blog"

[content]
  page_type = "post"
  count = 5
  order = "desc"

[design]
  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   4 = Citation (publication only)
  view = 2
+++
