---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Seminario De Videojuegos 2020"
event: Sala de Jitsi
event_url: https://meet.jit.si/SeminarioVideojuegosLidsol
summary: Grupo para explorar el diseño y desarrollo de videojuegos usando herramientas libres.
abstract: |-
    Taller dedicado a aprender diseño y desarrollo de videojuegos usando
    herramientas libres. Comenzaremos con programación de juegos con
    Common LISP y dedicaremos la última sesión de cada mes a abordar temas
    como música, motores libres, herramientas para creación de arte
    (*pixel art*, *voxel art*, etc), narrativa y el concepto de juego.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-06-06T19:00:00-05:00
date_end: 2020-12-19T21:00:00-06:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-06-04T21:54:40-05:00

authors: ["telior", "umoqnier", "emilio1625"]
tags: ["LISP", "Videojuegos", "Common LISP", "Godot", "Pixelorama", "Goxel", "Milkytracker", "Blender"]

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
#url_slides:

#url_code:
#url_pdf:
url_video: "https://www.youtube.com/watch?v=HM1Zb3xmvMc"

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
