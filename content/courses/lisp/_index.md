---
# Course title, summary, and position.
linktitle: Seminario de LISP
summary: Acércate a la programación funcional y a la computación con LISP :)

# Page metadata.
title: Seminario de LISP
date: "2020-06-01T19:00:00Z"
lastmod: "2020-06-02T22:59:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  lisp:
    name: Contenidos
    weight: 1
---

![Cartel del seminario](featured.png)

En este seminario leeremos el clásico SICP (*Structure and Interpretation of Computer Programs*),
     utilizado en el MIT durante décadas para iniciar a sus alumnos a la computación y a la programación empleando Scheme, un dialecto minimalista y elegante
     de la familia de lenguajes de programación LISP.